package pages;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

public class BasePage {
    public String BASE_URL = "https://api.trello.com";
    public String KEY ="c8c3a9d6aa15d864ae4209775c9eee49";
    public String TOKEN ="2edfcda7a68dcd9c217875be3ea62e33aa022ece9247ff7265772be617e37ffe";
    public String BOARDS ="/1/members/me/boards";
    public String ADD_LIST ="/1/list/";
    public String ADD_CARD ="/1/cards/";


    public JsonPath getElement(String key, String token, String endPoint){
        RestAssured.baseURI= BASE_URL;
        return given()
                .queryParam("fields", "name,url")
                .queryParam("key", key)
                .queryParam("token", token)
                .when().get(endPoint)
                .then().log().body().assertThat().statusCode(200).extract().jsonPath();
    }
    public void addElement(String key, String token, String endPoint, String name, String id){
        RestAssured.baseURI= BASE_URL;
        given().contentType(ContentType.JSON)
                .queryParam("key", key)
                .queryParam("token", token)
                .queryParam("name", name)
                .queryParam("idBoard", id)
                .when().post(endPoint)
                .then().log().body().assertThat().statusCode(200);
    }
    public void addElementOnList(String key, String token, String endPoint, String name, String id){
        RestAssured.baseURI= BASE_URL;
        given().contentType(ContentType.JSON)
                .queryParam("key", key)
                .queryParam("token", token)
                .queryParam("name", name)
                .queryParam("idList", id)
                .when().post(endPoint)
                .then().log().body().assertThat().statusCode(200);
    }
    public void addLabel(String key, String token, String name,String color, String endPoint){
        RestAssured.baseURI= BASE_URL;
        given().contentType(ContentType.JSON)
                .queryParam("fields", "name,url")
                .queryParam("key", key)
                .queryParam("token", token)
                .queryParam("name", name)
                .queryParam("color",color)
                .when().post(endPoint)
                .then().assertThat().statusCode(200);
    }
    public void archiveCardsOnList(String key, String token, String endPoint){
        RestAssured.baseURI= BASE_URL;
        given().contentType(ContentType.JSON)
                .queryParam("key", key)
                .queryParam("token", token)
                .when().post(endPoint)
                .then().log().body().assertThat().statusCode(200);
    }
    public void archiveOrUnarchiveList(String key, String token, String endPoint, Boolean isClosed){
        RestAssured.baseURI= BASE_URL;
        given().contentType(ContentType.JSON)
                .queryParam("key", key)
                .queryParam("token", token)
                .queryParam("closed", isClosed)
                .when().put(endPoint)
                .then().log().body().assertThat().statusCode(200);
    }
}

