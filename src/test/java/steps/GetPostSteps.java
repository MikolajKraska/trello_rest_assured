package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import pages.BasePage;

import javax.swing.*;


public class GetPostSteps {
    BasePage basePage = new BasePage();

    private Object BoardID;
    private Object ListaID;
    private Object CardID;

    @Given("GET information about boards")
    public void getBoardInfo() {
        JsonPath json = basePage.getElement(basePage.KEY,basePage.TOKEN, basePage.BOARDS);
        BoardID = (json.getList("id").get(0));
        System.out.println(BoardID);

        JsonPath json2 = basePage.getElement(basePage.KEY,basePage.TOKEN, "/1/boards/" + BoardID + "/lists");
        ListaID = (json2.getList("id").get(0));
        System.out.println(ListaID);
    }
    @And("Delete existing list")
    public void deleteExistingList() {
        basePage.archiveCardsOnList(basePage.KEY, basePage.TOKEN,"/1/lists/"+ ListaID + "/archiveAllCards");
        basePage.archiveOrUnarchiveList(basePage.KEY, basePage.TOKEN,"/1/lists/"+ ListaID + "/", true);
    }
    @When("Add new list to the board")
    public void addNewListToTheBoard() {
        basePage.addElement(basePage.KEY,basePage.TOKEN,basePage.ADD_LIST,"To jest najnowsza, ostatnio dodana lista", BoardID.toString());

        JsonPath json = basePage.getElement(basePage.KEY,basePage.TOKEN, "/1/boards/" + BoardID + "/lists");

        ListaID = (json.getList("id").get(0));
        System.out.println(ListaID);
    }
    @And("Add new card to the list")
    public void createNewCard() {
        basePage.addElementOnList(basePage.KEY,basePage.TOKEN,basePage.ADD_CARD,"zycie zycie jest nobelon", ListaID.toString());
        basePage.addElementOnList(basePage.KEY,basePage.TOKEN,basePage.ADD_CARD,"raz wygodne a raz prosze", ListaID.toString());
        basePage.addElementOnList(basePage.KEY,basePage.TOKEN,basePage.ADD_CARD,"wczoraj bialy bialy belon", ListaID.toString());
        basePage.addElementOnList(basePage.KEY,basePage.TOKEN,basePage.ADD_CARD,"a pojutrze znow kalosze", ListaID.toString());

        JsonPath json = basePage.getElement(basePage.KEY,basePage.TOKEN, "/1/boards/" + BoardID + "/cards");

        CardID = (json.getList("id").get(0));
        System.out.println(CardID);
    }
    @Then("Add label to the card")
    public void addLabelToTheCard() {
        basePage.addLabel(basePage.KEY,basePage.TOKEN,"to jest kolor niebieski","blue", "/1/cards/"+ CardID +"/labels");
        basePage.addLabel(basePage.KEY,basePage.TOKEN,"to jest kolor zielony","green", "/1/cards/"+ CardID +"/labels");
        basePage.addLabel(basePage.KEY,basePage.TOKEN,"to jest kolor zolty","yellow", "/1/cards/"+ CardID +"/labels");
        basePage.addLabel(basePage.KEY,basePage.TOKEN,"to jest kolor pomaranczowy","orange", "/1/cards/"+ CardID +"/labels");
        basePage.addLabel(basePage.KEY,basePage.TOKEN,"to jest kolor czerwony","red", "/1/cards/"+ CardID +"/labels");
    }
}